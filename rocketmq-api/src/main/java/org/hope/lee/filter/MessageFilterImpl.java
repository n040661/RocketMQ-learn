package org.hope.lee.filter;

import com.alibaba.rocketmq.common.filter.MessageFilter;
import com.alibaba.rocketmq.common.message.MessageExt;

public class MessageFilterImpl implements MessageFilter {

	@Override
	public boolean match(MessageExt msg) {
		// NO Chinese
        System.out.println("-------------");
        String property = msg.getUserProperty("SequenceId");
        System.out.println("---------" + property);
        if (property != null) {
            int id = Integer.parseInt(property);
            if((id % 2) == 0) {
            //if ((id % 3) == 0 && (id > 10)) {
                return true;
            }
        }
 
        return false;
	}

}
