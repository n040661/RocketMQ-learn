package org.hope.lee.producer.exception;

import com.alibaba.rocketmq.client.exception.MQBrokerException;
import com.alibaba.rocketmq.client.exception.MQClientException;
import com.alibaba.rocketmq.client.producer.DefaultMQProducer;
import com.alibaba.rocketmq.client.producer.SendCallback;
import com.alibaba.rocketmq.client.producer.SendResult;
import com.alibaba.rocketmq.common.message.Message;
import com.alibaba.rocketmq.common.message.MessageQueue;
import com.alibaba.rocketmq.remoting.exception.RemotingException;

public class ProducerException {
	public static void main(String[] args) {
		DefaultMQProducer producer = new DefaultMQProducer("push_consumer");
		producer.setNamesrvAddr("192.168.31.176:9876;192.168.31.165:9876");
		try {
			producer.setInstanceName("quick_start_producer");
			producer.setRetryTimesWhenSendFailed(3);
			producer.start();
			for(int i = 0; i < 10; i++) {
				Message msg = new Message("PushTopic_tt1", "TagA", "OrderID0034", ("message" + i).getBytes());
				//目前发现3.2.6版本没有这个方法，3.5.3版本有这个方法，并且必须要设置为false否则会报错
//				producer.setVipChannelEnabled(false);
				SendResult send = producer.send(msg);
				System.out.println("id:--->" + send.getMsgId() + ",result:--->" + send.getSendStatus());
			}
			
		} catch (MQClientException e) {
			e.printStackTrace();
		} catch (RemotingException e) {
			e.printStackTrace();
		} catch (MQBrokerException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		producer.shutdown();
	}
}
