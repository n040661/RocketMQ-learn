package org.hope.lee.producer.broadercast;

import com.alibaba.rocketmq.client.exception.MQBrokerException;
import com.alibaba.rocketmq.client.exception.MQClientException;
import com.alibaba.rocketmq.client.producer.DefaultMQProducer;
import com.alibaba.rocketmq.client.producer.SendCallback;
import com.alibaba.rocketmq.client.producer.SendResult;
import com.alibaba.rocketmq.common.message.Message;
import com.alibaba.rocketmq.common.message.MessageQueue;
import com.alibaba.rocketmq.remoting.exception.RemotingException;

public class ProducerBroadCast {
	public static void main(String[] args) {
		DefaultMQProducer producer = new DefaultMQProducer("push_consumer");
		producer.setNamesrvAddr("192.168.31.176:9876;192.168.31.165:9876");
		try {
			// 设置实例名称
			producer.setInstanceName("producer_broadcast");
			// 设置重试次数
			producer.setRetryTimesWhenSendFailed(3);
			// 开启生产者
			producer.start();
			// 创建一条消息
			Message msg = new Message("topic_broadcast", "TagA", "OrderID0034", "message_broadcast_test".getBytes());
			SendResult send = producer.send(msg);
			System.out.println("id:--->" + send.getMsgId() + ",result:--->" + send.getSendStatus());
			
		} catch (MQClientException e) {
			e.printStackTrace();
		} catch (RemotingException e) {
			e.printStackTrace();
		} catch (MQBrokerException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		producer.shutdown();
	}
}
